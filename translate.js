var https = require('https');
var fs = require('fs');

if (process.argv.length < 4) {
    return console.log('Usage: node translate <json_to_translate> <target_language_code>');
}

String.prototype.interpolate = function (object) {
    var pattern = /(#\{(.*?)\})/g;
    return this.replace(pattern, function() {
        var name = arguments[2];
        return typeof object[name] === 'string' ? object[name] : '';
    });
};

var input = process.argv[2];
var lang = process.argv[3];
var output = lang.charAt(0).toUpperCase() + lang.substr(1).toLowerCase() + '.json';

fs.readFile(input, onInputReady);

function onInputReady(err, data) {
    if (err) {
        return console.log(err);
    }
    var json = JSON.parse(data);
    var lineCount = Object.keys(json).length;
    var responseCount = 0;
    var translation = {};

    for (var id in json) {
        var request = https.request({
            method: 'GET',
            host: 'www.googleapis.com',
            port: '443',
            path: '/language/translate/v2?key=#{apikey}&target=#{lang}&q=#{line}'.interpolate({
                apikey: 'AIzaSyBax3idWhzTinHbLi6TEac1evK6huPrrOo',
                lang: lang,
                line: encodeURIComponent(json[id])
            })
        }, (function (id) {
            return function (response) {
                var str = '';
                response.on('data', function (chunk) {
                    str += chunk;
                });
                response.on('end', function () {
                    responseCount++;
                    process.stdout.write('Progress: ' + responseCount + ' out of ' + lineCount + '\033[0G');
                    var obj = JSON.parse(str);
                    translation[id] = obj.data ? obj.data.translations[0].translatedText : '###';
                    if (responseCount === lineCount) {
                        saveTranslation(translation);
                    }
                });
            }
        })(id)).on('error', function (e) {
            console.log('Request failed:', e.message);
        });
        request.end();
    }
}

function saveTranslation(translation) {
    fs.writeFile(output, JSON.stringify(translation, null, 4), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log('Translation has been saved as', output);
    });
}